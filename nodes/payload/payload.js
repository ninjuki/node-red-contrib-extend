"use strict";
module.exports = function(RED) {

   function Payload(config) {
      RED.nodes.createNode(this, config);

      var node = this;
      this.topic_v = config.topic_v;
      this.payload_v = config.payload_v;

      this.on('input', function (msg) {
            msg.payload = this.payload_v || msg.payload;
            msg.topic = this.topic_v || msg.topic;
            node.send(msg);
            node.status({ fill: "blue", shape: "dot", text: msg.payload });
      });

   }

   RED.nodes.registerType("payload", Payload);
}

