"use strict";
module.exports = function (RED) {

      var exec = require('child_process').exec;
      var isUtf8 = require('is-utf8');
      var upcast = require('upcast');

      function GpioRead(config) {
            RED.nodes.createNode(this, config);

            var node = this;
            this.rbe = config.rbe;
            this.pin = config.pin;
            this.invert = config.invert;

            node.previous = {};

            this.on('input', function (msg) {
                  var child;
                  var t = msg.topic || "_no_topic";

                  var cl = "gpio -1 read " + this.pin;
                  if (RED.settings.verbose) { node.log(cl); }
                  child = exec(cl, { encoding: 'binary', maxBuffer: 10000000 }, function (error, stdout, stderr) {
                        msg.payload = new Buffer(stdout, "binary");
                        // node.log("binary [" + msg.payload + "]");
                        if (isUtf8(msg.payload)) {
                              msg.payload = msg.payload.toString();
                              // node.log("Utf8 [" + msg.payload + "]");
                        }

                        if (upcast.is(msg.payload, 'string')) {
                              msg.payload = upcast.to(msg.payload, 'number');
                        }

                        if (node.invert === true) {
                              msg.payload = msg.payload === 1 ? msg.payload = 0 : msg.payload = 1;
                        }

                        if (node.rbe === true) {
                              if (msg.payload !== node.previous[t]) {
                                    node.previous[t] = msg.payload;
                                    node.send(msg);
                                    node.status({ fill: "blue", shape: "dot", text: msg.payload });
                              } else {
                                    node.status({ fill: "yellow", shape: "dot" });
                              }
                        } else {
                              node.send(msg);
                              node.status({ fill: "blue", shape: "dot", text: msg.payload });
                        }
                  });

            });

      }

      RED.nodes.registerType("gpio-read", GpioRead);
}

